function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("ROLES", {
	user: {
		duration : 900000,
		max : 15
	}
});