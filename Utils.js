var _ = require("underscore");

function Utils() {
    if(false === (this instanceof Utils)) {
        return new Utils();
    }
}

Utils.prototype.cleanElasticTweets = function(elasticResponse, uniqueUser) {

	// select field to be returned, flatten twitters nested structure and rename fields
    var mapped = _.map(elasticResponse.hits.hits, function(row){ 
		var mappedTweet = {
			tweet_id : row._source.id_str,
			text : (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.text : row._source.text,
			created_at : row._source.created_at,
			user_id :  row._source.user.id_str,
			screen_name : row._source.user.screen_name,
			name : row._source.user.name,
			profile_image_url : row._source.user.profile_image_url,
			country : row._source.user.country,
			rank : row._source.user.rank,
			in_reply_to_status_id : row._source.user.in_reply_to_status_id_str,
			rt_user_id: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.id_str : null,
			rt_screen_name: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.screen_name : null,
			rt_name: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.name : null,
			rt_profile_image_url: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.profile_image_url : null
		};
		return mappedTweet; 
	}),

	reduced = [],

	aggregations = {
		user_tweet_frequency : elasticResponse.aggregations.user_tweet_frequency,
		hashtags_freq : _.map(elasticResponse.aggregations.hashtags_freq.buckets, function(mapItem){ 
			mapItem.hastag_score = mapItem.unique_users.buckets.length;
			return mapItem; 
		})
	},

	response = {},

	count = elasticResponse.hits.total;

	if(uniqueUser !== undefined && uniqueUser === 'true') {
		// if uniqueuser flag is set then group by screen_name
		mapped = _.groupBy(mapped, function(o) {
		    return o.user_id;
		});
		reduced = _.reduce(mapped, function(reduced, mapItem){ 
			reduced.push(mapItem[0]);
			return reduced; 
		}, []);

		//sort based on latest timestamp
		reduced = _.sortBy(reduced, 'created_at').reverse();
	}
	else {
		reduced = mapped;
	}  

	response = {
		success : true,
		message : ((count > 0) ? "Record(s) Found" : "No Record(s) Found"),
		data : {
			totalCount : count,
			tweets : reduced,
			aggregations : aggregations
		}
	};
	/*process.nexttick(function(){
		cb(null, response);

		cb(new Error('whatevs'));
	})*/
	return(response);
};



Utils.prototype.cleanTrendingTweets = function(elasticResponse) {

	// select field to be returned, flatten twitters nested structure and rename fields
    var mapped = _.map(elasticResponse.hits.hits, function(row){ 
		var mappedTweet = {
			tweet_id : row._source.id_str,
			text : (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.text : row._source.text,
			created_at : row._source.created_at,
			user_id :  row._source.user.id_str,
			screen_name : row._source.user.screen_name,
			name : row._source.user.name,
			profile_image_url : row._source.user.profile_image_url,
			country : row._source.user.country,
			rank : row._source.user.rank,
			in_reply_to_status_id : row._source.user.in_reply_to_status_id_str,
			rt_user_id: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.id_str : null,
			rt_screen_name: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.screen_name : null,
			rt_name: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.name : null,
			rt_profile_image_url: (_.has(row._source, "retweeted_status") && !_.isNull(row._source.retweeted_status) ) ? row._source.retweeted_status.user.profile_image_url : null
		};
		return mappedTweet; 
	}),

	reduced = [],

	aggregations = {
		hashtags_freq : elasticResponse.aggregations.hashtags_freq.buckets,
		handle_freq : elasticResponse.aggregations.handle_freq.buckets
	},

	response = {},

	count = elasticResponse.hits.total;
	reduced = mapped;

	response = {
		success : true,
		message : ((count > 0) ? "Record(s) Found" : "No Record(s) Found"),
		data : {
			totalCount : count,
			tweets : reduced,
			aggregations : aggregations
		}
	};
	/*process.nexttick(function(){
		cb(null, response);

		cb(new Error('whatevs'));
	})*/
	return(response);
};

Utils.prototype.cleanShowList = function(response) {
	var mapped = _.groupBy(response, function(o) {
	    return o.c_id;
	});
	return mapped;
};

Utils.prototype.timestampToDate = function( ts ) {
	var d = new Date(ts * 1000);
	return (d.getFullYear() + "-" + this.zeroFill(d.getMonth()+1, 2) + "-" + this.zeroFill(d.getDay(), 2) + " " + this.zeroFill(d.getHours(), 2) + ":" +  this.zeroFill(d.getMinutes(), 2) + ":" + this.zeroFill(d.getSeconds(), 2));
};

Utils.prototype.zeroFill = function( number, width ) {
	width -= number.toString().length;
  	if ( width > 0 )  {
		return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  	}
  	return number + ""; // always return a string
};




module.exports = Utils;