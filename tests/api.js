var app = require('../app'),
    mysql =  require('mysql'),
    chai = require("chai"),
    assert = require("chai").assert,
    supertestChai = require('supertest-chai'),
    request = supertestChai.request,
    elasticsearch = require('elasticsearch'),
    validator = require('validator'),
    _ = require("underscore"),
    redis = require("redis");

chai.should();
chai.use(supertestChai.httpAsserts);
    
describe('api', function() {
    var mClient =  mysql.createConnection({
        host : "54.254.76.28",
        user : "abrar",
        password: "root123!"
    });
    var testUser = {
        name : "test",
        email : "test@test.com"
    };
    var testUser1 = {
        name : "test",
        email : "test1@test.com"
    };
    var test_c_id = 0;
    var test_show_id = 0;

    before(function(done) {
        mClient.connect();
        mClient.query("use tweetStore");
        mClient.query("DELETE FROM users WHERE name =" + mClient.escape(testUser.name) , function(err, rows, fields) {
            if(err)
                throw err;
            done();
        });
    });

    describe('signup', function() {
        it('should return success on new user signup', function(done) {
            request(app).post('/signupapi').send(testUser)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    res.body.email.should.equal(testUser.email);
                    mClient.query("SELECT * FROM users WHERE email =" + mClient.escape(testUser.email) , function(err, rows, fields) {
                        assert.isNull(err, "No errors from mysql");
                        assert.lengthOf(rows, 1, "testuser found in mysql");
                        testUser.apikey = rows[0].apikey;
                        done();
                    });
                });
        });

        it('should return error on invaid email', function(done) {
            var baduser = {
                name : 'test',
                email : "bademail"
            }
            request(app).post('/signupapi').send(baduser)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                    done();
                });
        });

        it('should return error on missing/incorrent argument', function(done) {
            var baduser = {
                name1 : 'test',
                email : "bademail"
            }
            request(app).post('/signupapi').send(baduser)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                    done();
                });
        });
    });

    describe('resendemail', function() {
        it('should return success on resendemail to valid email', function(done) {
            request(app).get('/resendEmail?email='+testUser.email)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    res.body.email.should.equal(testUser.email);
                    done();
                });
        });

        it('should return error on invaid email', function(done) {
            request(app).get('/resendEmail?email=testingwrongemail')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                });

            request(app).get('/resendEmail?email=')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                    done();
                });
        });
    });

    describe('getTweets', function() {

        it('should return failiure on passing not params', function(done) {
            var getParams = {
            };
            request(app).get('/getTweets')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                    done();
                });
        });

        it('should return failiure on just passing top size', function(done) {
            request(app).get('/getTweets?top=0&size=50')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(false);
                    res.should.have.status(400);
                    done();
                });
        });

        it('should return sucess on passing top size high low', function(done) {
            request(app).get('/getTweets?top=0&size=50&high=1000&low=750')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);

                    _.each(res.body.data.tweets, function(tweet) {
                        if(tweet.rank < 750 || tweet.rank > 1000)
                            assert.notOk(tweet, "rank range dint work");
                    });
                    done();
                });
        });

        it('should return sucess on passing top size high low country', function(done) {
            request(app).get('/getTweets?top=0&size=50&high=1000&low=750&country=in')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    if(res.body.data.tweets.length > 0) {
                        var country = _.uniq(_.pluck(res.body.data.tweets, 'country'));
                        if(country.length != 1 || country[0] != 'in')
                            assert.notOk(country, "other country found");
                    }
                    done();
                });
        });

        it('should return sucess on passing top size high low uniqueUser ', function(done) {
            request(app).get('/getTweets?top=0&size=50&high=1000&low=750&country=in&uniqueUser=true')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);

                    var user_ids = _.uniq(_.pluck(res.body.data.tweets, 'user_id'));
                    if(user_ids.length !== res.body.data.tweets.length)
                        assert.notOk(user_ids, "uniqueUser function did not work");

                    done();
                });
        });

        it('should return sucess on passing top size high low since_ts ', function(done) {
            var date = new Date();
            date = validator.toInt(date.setHours(date.getHours()-1)/1000);
            request(app).get('/getTweets?top=0&size=50&high=1000&low=750&country=in&since_ts='+date)
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);

                    _.each(res.body.data.tweets, function(tweet) {
                        var d = validator.toInt(Date.parse(tweet.created_at)/1000);
                        if(d > date)
                            assert.notOk(tweet, "since_ts function did not work");
                    });

                    done();
                });
        });

        it('should return sucess on passing top size high low screen_name ', function(done) {
            request(app).get('/getTweets?top=0&size=50&high=1000&low=750&screen_name=timesofindia&country=in')
                .end(function(err,res) {
                    if (err) {
                        assert.notOk(err);
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    if(res.body.data.tweets.length > 0) {
                        var screen_names = _.uniq(_.pluck(res.body.data.tweets, 'screen_name'));
                        if(screen_names.length != 1 || screen_names[0] != 'timesofindia')
                            assert.notOk(screen_names, "other screen_names found");
                    }
                    done();
                });
        });
    });

    describe('getChnl', function() {
        it('should return success on calling getChnl with correctkey', function(done) {
            request(app).get('/api/getChnl?apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    if(res.body.data.length > 0)
                        test_c_id = res.body.data[0].c_id;
                    done();
                });
        });

        it('should return success on calling getChnl with wrong key', function(done) {
            request(app).get('/api/getChnl?apikey=junk')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });
    });

    describe('getShows', function() {
        it('should return success on calling getShows with correctkey', function(done) {
            request(app).get('/api/getShows?apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    test_show_id = res.body.data[test_c_id][0].showId;
                    done();
                });
        });

        it('should return success on calling getShows with wrong key', function(done) {
            request(app).get('/api/getShows?apikey=junk')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });
    });

    describe('getChnlTweets', function() {
        it('should return success on calling getChnlTweets with correct key and c_id', function(done) {
            request(app).get('/api/getChnlTweets?c_id='+test_c_id+'&apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    done();
                });
        });

        it('should return fail on calling getChnlTweets with wrong key', function(done) {
            request(app).get('/api/getChnlTweets?c_id='+test_c_id+'&apikey=junk')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });

        it('should return fail on calling getChnlTweets with correct key and missing c_id', function(done) {
            request(app).get('/api/getChnlTweets?apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.should.have.status(400);
                    done();
                });
        });

        it('should return fail on calling getChnlTweets with no params', function(done) {
            request(app).get('/api/getChnlTweets')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });
    });

    describe('getShowTweets', function() {
        it('should return success on calling getShowTweets with correct key and showid', function(done) {
            request(app).get('/api/getShowTweets?showid='+test_show_id+'&apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.body.success.should.equal(true);
                    res.should.have.status(200);
                    done();
                });
        });

        it('should return fail on calling getShowTweets with wrong key and correct showid', function(done) {
            request(app).get('/api/getShowTweets?showid='+test_show_id+'&apikey=junk')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });

        it('should return fail on calling getShowTweets with correct key and missing showid', function(done) {
            request(app).get('/api/getShowTweets?apikey='+testUser.apikey)
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.be.json;
                    res.should.have.status(400);
                    done();
                });
        });

        it('should return fail on calling getShowTweets with no params', function(done) {
            request(app).get('/api/getShowTweets')
                .end(function(err,res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.status(302);
                    done();
                });
        });
    });

    after(function(done) {
        mClient.query("DELETE FROM users WHERE name =" + mClient.escape(testUser.name) , function(err, rows, fields) {
            if(err)
                throw err;
        });
        mClient.end(function(err) {
            done();
        });
    });
});

describe('databases', function() {
    describe('mysql', function() {
        it('should return success on mysql connect and close', function(done) {
            var mClient = mysql.createConnection({
                host : "54.254.76.28",
                user : "abrar",
                password: "root123!"
            });
            mClient.connect(function(err) {
                assert.equal(err, null, "mysql conn successfull");
            });
            mClient.end(function(err) {
                assert.equal(err, null, "mysql conn successfull closed");
                done();
            });
        });
    });

    describe('elasticsearch', function() {
        it('should return success on elasticsearch connect and close', function(done) {
            var eClient = elasticsearch.Client({
                hosts: [
                    'http://54.254.76.28:9200'
                ]
            });
            eClient.ping({
                requestTimeout: 1000,
                hello: "elasticsearch!"
            }, function (error) {
                assert.isUndefined(error, "Elastic Search is up");
                done();
            });
        });
    });

    describe('redis', function() {
        it('should return success on redis connect and close', function(done) {
            var rClient = redis.createClient();

            rClient.on("connect", function (err, res) {
                assert.isUndefined(err, "Redis Connected successfully");
                rClient.quit(function (err, res) {
                    assert.isNull(err, "Redis quit successfully");
                    done();
                });
            });

            rClient.on("error", function (err, res) {
                assert.notOk(err, "Redis Failed");
                done();
            });
        });
    });
});