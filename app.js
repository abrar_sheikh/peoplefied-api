var express = require('express'),
	app = express(),
	elasticsearch = require('elasticsearch'),
	_ = require("underscore"),
	Utils = require("./Utils"),
	passport = require('passport'),
	LocalStrategy = require('passport-localapikey').Strategy,
	redis = require("redis"),
    rClient = redis.createClient(),
    Limiter = require("ratelimiter"),
    mysql =  require('mysql'),
    hat = require('hat'),
    validator = require('validator'),
    mail  = require("nodemailer").mail,
    constants = require("./constants");



module.exports = app;

var utils = new Utils();

var eClient = elasticsearch.Client({
	hosts: [
		'http://54.254.76.28:9200'
	]
});

var mClient =  mysql.createConnection({
  	host : "54.254.76.28",
  	user : "abrar",
  	password: "root123!"
});
mClient.connect();

function findById(id, fn) {
	mClient.query("use tweetStore");
	mClient.query("SELECT * FROM users WHERE id =" + mClient.escape(id) , function(err, rows, fields) {
		if(err) return fn(err);
		if(rows.length === 0) return fn(null, null);
		return fn(err, rows[0]);
	});
}

function findByApiKey(apikey, fn) {
	rClient.get(apikey, function(err, reply) {
      	if(err !== null && reply) {
      		return fn(err, JSON.parse(reply));
      	}
    });
	mClient.query("use tweetStore");
	mClient.query("SELECT * FROM users WHERE apikey =" + mClient.escape(apikey) , function(err, rows, fields) {
		if(err) return fn(err);
		if(rows.length === 0) return fn(null, null);
		if(rows.length === 1)
			rClient.set(rows[0].apikey, JSON.stringify(rows[0]));
		return fn(err, rows[0]);
	});
}
 
function findByEmail(email, fn) {
	mClient.query("use tweetStore");
	mClient.query("SELECT * FROM users WHERE email =" + mClient.escape(email) , function(err, rows, fields) {
		return fn(err, rows);
	});
}

passport.serializeUser(function(user, done) {
  	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  	findById(id, function (err, user) {
    	done(err, user);
  	});
});

passport.use(new LocalStrategy(
  function(apikey, done) {
    process.nextTick(function () {
      
      findByApiKey(apikey, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Unknown apikey : ' + apikey }); }
       // if (user.password != password) { return done(null, false, { message: 'Invalid password' }); }
        return done(null, user);
      });
    });
  }
));

// configure Express
app.configure(function() {
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.session({ secret: '_brizztv_api' }));
  // Initialize Passport!  Also use passport.session() middleware, to support
  // persistent login sessions (recommended).
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);

});

function validateSignup(req, res, next) {
	var err = {
		success : false,
		message : []
	};
	res.type('application/json');
	if(!validator.isJSON(JSON.stringify(req.body)) || req.body.name === undefined || req.body.email === undefined) {
		err.message.push("invalid request");
	}
	else {
		if(validator.trim(req.body.name) === '')
			err.message.push("Name cannot be empty");
		if(validator.trim(req.body.email) === '')
			err.message.push("Email cannot be empty");
		if(!validator.isEmail(req.body.email))
			err.message.push("Invalid Email syntax");
	}

	if(err.message.length > 0)
		res.send(400, JSON.stringify(err));
	else  {
		return next();
	}
}

function validateResendEmail(req, res, next) {
	var err = {
		success : false,
		message : []
	};
	res.type('application/json');
	if(req.query.email === undefined) {
		res.send(400, "Invalid request");
	}
	if(validator.trim(req.query.email) === '')
		err.message.push("Email cannot be empty");
	if(!validator.isEmail(req.query.email))
		err.message.push("Invalid Email syntax");

	if(err.message.length > 0)
		res.send(400, JSON.stringify(err));
	else  {
		return next();
	}
}

function chkLimit(req, res, next) {
	var id = req.user.id;
	var role = _.pick(constants.ROLES, req.user.role );
	var limit = new Limiter({ id: id, db: rClient, duration : 900000, max : 15 });
	limit.get(function(err, limit){
	  if (err) return next(err);

	  res.set('X-RateLimit-Limit', limit.total);
	  res.set('X-RateLimit-Remaining', limit.remaining);
	  res.set('X-RateLimit-Reset', limit.reset);
	  // all good
	  /*debug('remaining %s/%s %s', limit.remaining, limit.total, id);*/
	  if (limit.remaining) return next();

	  // not good
	  var delta = (limit.reset * 1000) - Date.now() | 0;
	  var after = limit.reset - (Date.now() / 1000) | 0;
	  res.set('Retry-After', after);
	  res.send(429, 'Rate limit exceeded, retry in ' + ms(delta, { long: true }));
	});
}

function  chkShowTweetParams(req, res, next) {
	res.type('application/json');
	if(req.query.showid  === undefined ||
		req.query.showid.trim() === '')
			res.send(400, {
				success	 : false,
				message: "Invalid Request Parameter"
			});
	else
		next();
}

function  chkChnlTweetParams(req, res, next) {
	res.type('application/json');
	if(req.query.c_id  === undefined ||
		req.query.c_id.trim() === '')
			res.send(400, {
				success	 : false,
				message: "Invalid Request Parameter"
			});
	else
		next();
}

function setup(req, res, next) {
	res.type('application/json');
	next();
}
//passport.authenticate('localapikey', { failureRedirect: '/api/unauthorized', session:false}),chkLimit,

module.exports.getTweets = function (req, res) {
	res.type('application/json');
	var getParams = req.query,
		sortOrder = 'desc',
		chkFlag = false,
		queryArr = [],
		query = {},
		response = {};

	if(getParams.top  !== undefined && 
		getParams.size  !== undefined &&
		getParams.top.trim() !== '' &&
		getParams.size.trim() !== '') {

		if(getParams.high  !== undefined && 
			getParams.low  !== undefined &&
			getParams.high.trim() !== '' &&
			getParams.low.trim() !== '') {
            chkFlag = true;
        	queryArr.push({
        		range : {
                    rank : {
                        from : getParams.low,
                        to : getParams.high,
                        include_upper : true
                    }
                }
        	});
		}

		if(getParams.q  !== undefined && 
			getParams.q.trim() !== '') {

            chkFlag = true;
        	queryArr.push({
        		query : {
                    term : {
                        text : getParams.q.trim()
                    }
                }
        	});
		}

		if(getParams.screen_name  !== undefined && 
			getParams.screen_name.trim() !== '') {

            chkFlag = true;
        	sortOrder = "asc";
        	queryArr.push({
        		query : {
                    term : {
                        screen_name : getParams.screen_name.trim()
                    }
                }
        	});
		}

		queryArr.push({
    		missing : {
                field : 'in_reply_to_status_id',
                existence : true,
                null_value :  true
            }
    	});

		if(getParams.country  !== undefined && 
			getParams.country.trim() !== '') {
            chkFlag = true;

        	queryArr.push({
        		query : {
                    term : {
                        "user.country" : getParams.country.trim()
                    }
                }
        	});
		}

		if(getParams.since_ts  !== undefined && 
			getParams.since_ts.trim() !== '') {

			utils.timestampToDate	(getParams.since_ts);

        	queryArr.push({
        		range : {
                    created_at : {
                        gt : utils.timestampToDate(getParams.since_ts)
                    }
                }
        	});
		}

		if(getParams.topic  !== undefined && 
			getParams.topic.trim() !== '') {
            chkFlag = true;
    
			eClient.mget({
			  index: 'tweetstore-meta',
			  type: 'topics',
			  body: {
			    ids: [getParams.topic]
			  }
			}, function(error, response){
				var users = response.docs[0]._source.users;
				var keywords = response.docs[0]._source.keywords;

				if(users !== null && users.length > 0) {
					if(getParams.topic == 1) {
						queryArr.push({
			        		query : {
			                    terms : {
			                        "user.screen_name" : users,
			                        minimum_should_match : 1
			                    }
			                }
		        		});
					}
					else {
						queryArr.push({
			        		query : {
			                    terms : {
			                        "user.id_str" : users,
			                        minimum_should_match : 1
			                    }
			                }
		        		});
					}
				}

				if(keywords !== null && keywords.length > 0) {
					queryArr.push({
		        		query : {
		                    terms : {
		                        text : keywords
		                    }
		                }
	        		});
				}
			});
		}
	} else {
		res.send(400, {
			success : false,
			message : 'Invalid Query Parameters'
		});
		return;
	}
	if(chkFlag === false) {
		res.send(400, {
			success : false,
			message : 'Invalid Query Parameters'
		});
		return;
	}

	query = {
		query : {
            filtered : {
                filter : {
                    and : queryArr
                }
            }
        },
        from : getParams.top.trim(),
        size : getParams.size.trim(),
        sort : {
            created_at : {
                order : sortOrder
            }
        },
        aggregations : {
            hashtadgs_freq : [
	            {
	                terms : {
	                    field : "text",
	                    include : "(#[a-zA-Z][a-zA-Z0-9_-]+)",
	                    size  : 0
	                },
	                aggregations : {
	                    unique_users : {
	                        terms : {
	                            field : "user.screen_name",
	                            size: 0
	                        }
	                    }
	                }
	            }
            ],
            user_tweet_frequency  : {
                terms :  {
                    field : "user.screen_name",
                    size  : 0
                }
            }
        }
	};
        
    
	eClient.search({
		index: 'ts',
		body: query,
		}).then(function (resp) {
			var tweets = utils.cleanElasticTweets(resp, getParams.uniqueUser);
			res.send(tweets);

		}, function (err) {				
			console.trace(err.message);
	});
};

app.get('/api/getTweets',  app.getTweets);

app.get('/tweet-store/index.php/api/TweetsUnique/get' , app.getTweets);

app.post('/signupapi', validateSignup,  function (req, res) {
	var user  = { name: req.body.name, email: req.body.email, apikey:hat(), role : "user" };

	findByEmail(user.email, function(err, rows){
		
	   	if(err) {
	    	res.send( {
	    		success: false,
	    		message : "Failed"
	    	} );
	  		return;
	  	}
	 
	    if(rows.length > 0)
	    	res.send( {
	    		success: false,
	    		message : "Email "+rows[0].email+ " is Already registered"
	    	} );
	    else {
	    	mClient.query('INSERT INTO users SET ?', user, function(err, result) {
			  	if(err) {
			    	res.send( {
			    		success: false,
			    		message : "Failed"
			    	} );
			  		return;
			  	}
			  	mail({
				    from: "api@brizzlabs.com", // sender address
				    to: user.email, // list of receivers
				    subject: "BrizzTV API Key", // Subject line
				    text: "The api key to access brizzTv resourse is " + user.apikey 
				});
			  	res.send( {
		    		success: true,
		    		message : "User Created",
		    		email : user.email
		    	});
			});
	    }
	});
});

app.get('/resendEmail', validateResendEmail,  function (req, res) {
	var user  = {email: req.query.email};

	findByEmail(user.email, function(err, rows){
		
	   	if(err) {
	    	res.send( {
	    		success: false,
	    		message : "Failed"
	    	} );
	  		return;
	  	}
	 
	    if(rows.length > 0) {
	    	mail({
			    from: "api@brizzlabs.com", // sender address
			    to: user.email, // list of receivers
			    subject: "BrizzTV API Key", // Subject line
			    text: "The api key to access brizzTv resourse is " + rows[0].apikey 
			});
		  	res.send( {
	    		success: true,
	    		message : "Email Sent",
	    		email : user.email
	    	});

	    }
	    else {
	    	res.send( {
	    		success: false,
	    		message : "Email not Registered"
	    	} );
	    }
	});
});

app.get('/api/getChnl',setup,  passport.authenticate('localapikey', { failureRedirect: '/api/unauthorized', session:false}),chkLimit, function(req, res) {
	mClient.query("use showdashboard_db");
	mClient.query("SELECT c.c_id, c.c_name FROM tvchannels AS c;" , 
			function(err, rows, fields) {
		if(err) 	//handle these errors
			res.send(500, {
				success : false,
				message : "Database Error"
			})
		res.send({
			success : true,
			data : rows
		});
	});
});

app.get('/api/getShows',setup, passport.authenticate('localapikey', { failureRedirect: '/api/unauthorized', session:false}),chkLimit, function(req, res) {
	mClient.query("use showdashboard_db");
	mClient.query("SELECT s.showId, s.showName, s.showType, s.showGenre, s.showLang, s.showDesc, s.showCast, c.c_id, c.c_name FROM  	`tvshows` AS s, tvchannels AS c	WHERE 	s.c_id = c.c_id" , 
			function(err, rows, fields) {
		if(err) 	//handle these errors
			res.send(500, "Database Error")
		res.send({
			success : true,
			data : utils.cleanShowList(rows)
		});
	});
});

app.get('/api/getChnlTweets',setup, passport.authenticate('localapikey', { failureRedirect: '/api/unauthorized', session:false}),chkLimit, chkChnlTweetParams, function(req, res) {
	mClient.query("use showdashboard_db");
	var limit = {
		lower : 0,
		upper : 49
	};
	var page = 0;
	if(req.query.page  !== undefined &&
		validator.isInt(req.query.page)) {
		page = validator.toInt(req.query.page);
		limit.lower = 50*page;
		limit.upper = limit.lower + 50*page - 1;
	};
	var query = "SELECT t.id, t.text, t.from_user AS screen_name, t.from_user_name AS name, t.tweet_created_at AS created_at, profile_image_url FROM tweet_store AS t, chnltopost AS p WHERE t.id = p.postid AND p.c_id ="+req.query.c_id + " LIMIT "+limit.lower+","+limit.upper;
	
	mClient.query(query , 
			function(err, rows, fields) {
		if(err) 	//handle these errors
			res.send(500, "Database Error")
		res.send({
			success : true,
			data : rows
		});
	});
});

app.get('/api/getShowTweets',setup, passport.authenticate('localapikey', { failureRedirect: '/api/unauthorized', session:false}),chkLimit, chkShowTweetParams, function(req, res) {
	mClient.query("use showdashboard_db");
	var limit = {
		lower : 0,
		upper : 49
	};
	var page = 0;
	if(req.query.page  !== undefined &&
		validator.isInt(req.query.page)) {
		page = validator.toInt(req.query.page);
		limit.lower = 50*page;
		limit.upper = limit.lower + 50*page - 1;
	};
	var query = "SELECT t.id, t.text, t.from_user AS screen_name, t.from_user_name AS name, t.tweet_created_at AS created_at, profile_image_url FROM tweet_store AS t, showtopost AS p WHERE t.id = p.postid AND p.showid ="+req.query.showid + " LIMIT "+limit.lower+","+limit.upper;

	mClient.query(query , 
			function(err, rows, fields) {
		if(err) 	//handle these errors
			res.send(500, "Database Error")
		res.send({
			success : true,
			data : rows
		});
	});
});

app.get('/api/unauthorized',setup, function(req, res) {
	res.send(403, {
		success : false,
		message : "unauthorized"
	});
});

app.get('/api/trending',  function (req, res) {
	res.type('application/json');
	var getParams = req.query;
	query = {
		query : {
            filtered : {
                filter : {
                    and : [{
                    	query : {
				            term : {
				                screen_name : getParams.screen_name.trim()
				            }
				        }
                    }]
                }
            }
        },		
        from : 0,
        size : 100,
        sort : {
            created_at : {
                order : "desc"
            }
        },
        aggregations : {
            hashtags_freq : [
	            {
	                terms : {
	                    field : "text",
	                    include : "(#[a-zA-Z][a-zA-Z0-9_-]+)",
	                    size  : 0
	                },
	            }
            ],
            handle_freq : [
	            {
	                terms : {
	                    field : "text",
	                    include : "(@[a-zA-Z][a-zA-Z0-9_-]+)",
	                    size  : 0
	                },
	            }
            ],
        }
	};
        
    
	eClient.search({
		index: 'ts',
		body: query,
		}).then(function (resp) {
			res.send(utils.cleanTrendingTweets(resp));
		}, function (err) {				
			console.trace(err.message);
	});
});

app.listen(process.env.PORT || 4730);